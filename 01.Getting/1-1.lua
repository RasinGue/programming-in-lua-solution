#!/usr/bin/lua
-- file: 1-1.lua
-- Exercise 1.1: Run the factorial example. What happens
-- to your program if you enter a negative number? Modify
-- the example to avoid this problem.

-- when entering a negative number in the factorial example,
-- it keeps going until it stops due to a stack overflow.
-- The remedy is to check for negative numbers in the 
-- function.

-- defines a factorial function
-- returns nil on error, the factorial otherwise

function fact (n)
	if n < 0 then
		return nil
	elseif n == 0 then
		return 1
	else
		return n * fact(n - 1)
	end
end

print("Please enter the number")
a = io.read("*n")

-- check for error
if fact == nil then
    print("Maybe you have entered a negtive number?")
else
    print(fact(a))
end
