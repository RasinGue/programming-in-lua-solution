#!/usr/bin/lua
-- file: 1-2.lua
-- Exercise 1.2: Run the 'twice' example, both by loading the file with 
-- the -l option and with `dofile`.
-- Which way do you prefer?

-- -l option is convenient in command line lua while `dofile` is
-- a quicker way in the lua file

dofile("twice.lua")
