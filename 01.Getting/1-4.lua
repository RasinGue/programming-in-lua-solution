#!/usr/bin/lua
-- file: 1-4.lua
-- Exercise 1.4: Which of the following  strings are
-- valid identifiers?
-- ___  _end  End  end  until?  nil NULL

print("until?\t--> question marks are not allowed")
print("end\t--> reserved keyword")
print("nil\t--> reserved type name")
print("___\t--> valid")
print("_end\t--> valid")
print("End\t--> valid")
print("NULL\t--> valid")
