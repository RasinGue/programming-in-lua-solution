#!/usr/bin/lua
-- file: name.lua
-- Exercise 1.5: Write a simple script that prints
-- it's own name without knowing it in advance.

-- the script name is always the first parameter in arg

print("The filename is: " .. arg[0])
