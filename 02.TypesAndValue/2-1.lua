#!/usr/bin/lua

-- file: 2-1.lua
-- Exercise 2.1: What is the value of the expression
-- type(nil)==nil? (You can use Lua to check your
-- answer.) Can you explain this result?

print(type(nil) == nil)

print(type(nil))
print(type(type(nil)))

-- type(nil) is a string while nil is just nil type
-- so type(nil) ~= nil
