#!/usr/bin/lua

-- file: 2-1.lua
-- Exercise 2.2: Which of the following are valid
-- numerals? What are their values?

print(".e12\t--> invalid (no decimal part specified)")
print("0.0e\t--> invalid (no exponent specified)")
print("0xABFG\t--> invalid (no G in hexadecimal)")
print("FFFF\t--> invalid (no hex identifier)")
print("0x\t--> invalid (no number specified")
print(".0e12\t--> valid " ..  .0e12)
print("0x12\t--> valid " .. 0x12)
print("0xA\t--> valid " .. 0xA)
print("0xFFFFFFFF --> valid " .. 0xFFFFFFFF)
print("0x1P10\t--> valid " .. 0x1P10)
print("0.1e1\t--> valid " .. 0.1e1)
print("0x0.1p1\t--> valid " .. 0x0.1p1)
