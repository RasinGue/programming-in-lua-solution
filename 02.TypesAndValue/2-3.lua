#!/usr/bin/lua
-- file: 2-3.lua
-- Exercise 2.3: The number 12.7 is equal to the fraction
-- 127/10, where the denominator is a power of ten. Can 
-- you express it as a common fraction where the denominator
-- is a power of two? What about the number 5.5?

function represent(number, devidend)
    print("Represent "..number)
    denominator = 1
    for i = 1, devidend do
        denominator = 2 * denominator
        print((number*denominator) .. '/' .. denominator)
    end
    print()
end

represent(12.7, 10)

represent(5.5, 10)
