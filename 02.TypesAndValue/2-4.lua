#!/usr/bin/lua
-- file: 2-4.lua
-- Exercise 2.4: How can you embed the following piece
-- of XML as a string in Lua? Show at least two
-- different ways.

xmldata1 = [=[
<![CDATA[
  Hello World
]]>
	]=]

xmldata2 = "<![CDATA[\n  Hello World\n]]>"

print(xmldata1)
print(xmldata2)
