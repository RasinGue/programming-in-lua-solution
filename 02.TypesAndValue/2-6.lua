#!/usr/bin/lua
-- file: 2-6.lua
-- Exercise 2.6: Assume the following code:
--     a = {};  a.a = a
-- What would be the value of a.a.a.a? Is any
-- a in that sequence somehow different from
-- the others?
-- Now, add the next line to the previous line:
--     a.a.a.a = 3
-- What would be the value of a.a.a.a now?

a = {}
a.a = a
print("a", a)
print("a.a", a.a)
print("a.a.a", a.a.a)
print("a.a.a.a", a.a.a.a)

-- Shows the address of table `a`

a.a.a.a = 3

print("a.a.a.a", a.a.a.a)

-- Run-time error
