#!/usr/bin/lua
-- file: exponents.lua
-- Exercise 3.2: What is the result of the expression 2^3^4?
-- What about 2^-3^4?

print(2^3^4)

print(2^-3^4)

-- 2.4178516392293e+24
-- 4.1359030627651e-25
