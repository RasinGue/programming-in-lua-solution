#!/ust/bin/lua
-- file: polynomials.lua
-- Exercise 3.3: We can represent a polynomial in Lua as a list
-- of coefficients, such as {a0, a1, ..., an}.
-- Write a function that receives a polynomial (represented as
-- a table) and a value for x and returns the polynomial value.

function poly(c, x) 
	local sum = 0
	for i, v in ipairs(c) do
		sum = sum + v * x ^ (#c - i)
	end 
	return sum 
end

print("How many coefficients do you need?")
size = io.read("*n")

print("Please enter the coefficients:")
c={}

for i = 1, size do
    c[#c+1] = io.read("*n")
end

print("please enter x values (ctrl-d to exit)")
while true do
    x = io.read("*n")
    print("The answer = " .. poly(c, x))
end

print(poly(a, 10))
