#!/usr/bin/lua
-- file: 3-4.lua
-- Exercise 3.4: Can you write a function from the previous
-- item so that it uses at most n additions and n multiplications
-- (and no exponentiations)?


function nonpoly(c, x) 
	local sum = 0 cur = 1
	for i = #c, 1, -1 do
        sum = sum + cur*c[i]
        cur = cur * x
    end
	return sum 
end

print("How many coefficients do you need?")
size = io.read("*n")

print("Please enter the coefficients:")
c={}

for i = 1, size do
    c[#c+1] = io.read("*n")
end

print("please enter x values (ctrl-d to exit)")
while true do
    x = io.read("*n")
    print("The answer = " .. nonpoly(c, x))
end

print(nonpoly(a, 10))
