#!/usr/bin/env lua
-- file: 3-5.lua
-- Exercise 3.5: How can you check whether a value is boolean
-- without using the type function?

function is_boolean(v) 
	return v == true or v == false 
end

