#!/usr/bin/lua
-- file: 3-6.lua
-- Exercise 3.6: Consider the following expression:
--     (x and y and (not z)) or ((not y) and x)
-- Are the parentheses necessary? Would you recommend
-- their use in that expression?

x = false
y = true
z = false

print((x and y and (not z)) or ((not y) and x))

print((x and y and not z) or ((not y) and x))

print(x and y and not z or not y and x)

-- There is no need to use these parentheses
-- first the not is evaluated (highest precedence),
-- then the and part is evaluated (below not but higher than or),
-- finally the or part is evaluated (lowest precedence)



