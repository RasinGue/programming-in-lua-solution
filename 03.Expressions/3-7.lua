#!/usr/bin/lua
-- file: days.lua
-- Exercise 3.7: What will the following print?
--     sunday = "monday"; monday = "sunday"
--     t = {sunday = "monday", [sunday] = monday}
--     print(t.sunday, t[sunday], t[t.sunday])
sunday = "monday" 
monday = "sunday"
t = {
	sunday = "monday", 
	[sunday] = monday, 
}

print(t.sunday, t[sunday], t[t.sunday])
-- t.sunday = "monday"
-- t[sunday] = monday = "sunday"
-- t[t.sunday] = t["monday"] = t.monday = t[sunday] = monday = "sunday"
