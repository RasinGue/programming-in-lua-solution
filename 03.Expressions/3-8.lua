#!/usr/bin/lua
-- file: 3-8.lua
-- Exercise 3.8: Suppose that you want to create a table
-- that associates each escape sequence for strings with
-- it's meaning. How could you write a constructor for
-- that table?

escapes = {
    ["\n"] = "newline",
    ["\r"] = "carriage return",
    ["\a"] = "alert"
}
