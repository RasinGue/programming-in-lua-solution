#!/usr/bin/lua

-- file: 4-1.lua
-- Exercise 4.1: Most language wih a C-like syntax
-- do not offer an elseif construct. Why does Lua
-- need this construct more than those languages?

-- Because Lua doesn't offer `switch` function 
-- Once there are more than one selection, if..then..else..
-- statement would be very difficult to handle
-- So else if make a great effort to make more selection 
-- in one `if` statement
