#!/usr/bin/lua

-- file: 4-2.lua
-- Exercise 4.2: Describe four different ways to write 
-- an unconditional loop in Lua. Which one do you prefer?

function whileloop()
	while true do
		print("looping")
	end
end

function repeatloop()
	repeat
		print("looping")
	until false
end

function forloop()
	for i = 1, math.huge do
		print("looping")
	end
end

function gotoloop()
	::loop::
	print("looping")
	goto loop
end


