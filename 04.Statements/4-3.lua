#!/usr/bin/lua

-- file: 4-3.lua
-- Exercise 4.3: Many people argue that repeat-until is 
-- seldom used, and therefore it should not be present in
-- a minimalistic like Lua. What do you think?

-- `repeat-until` statement is usually used under those
-- circumstance which at least excute the loop one time

-- But in fact I seldom use this statement ;)
