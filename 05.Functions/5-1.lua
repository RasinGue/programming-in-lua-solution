#!/usr/bin/lua
-- file: 5-1.lua
-- Exercise 5.1: Write a function that receives an arbitrary number
-- of strings and returns all of them concatenated together

function concatenate(...)
	args = table.pack(...) --Returns a new table with all parameters stored into keys 1, 2, etc. and with a field "n" with the total number of parameters.
	if(args.n == 0) then
		return "No arguments!"
	end
	
	str = tostring(args[1])
	for i = 2, args.n do
		str = str .. ' ' .. tostring(args[i])
	end
	return str
end

print(concatenate("Programming", "in", "Lua"))
print(concatenate())
