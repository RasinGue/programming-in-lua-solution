#!/usr/bin/lua
-- file: 5-2.lua
-- Exercise 5.2: Write a function that receives an array 
-- and prints all elements in that array. Consider the 
-- pros and cons of using table.unpack in this function

function arrayprint(a)
    for i, v in pairs(a) do
        print(tostring(v))
    end
end

a = {firstname="John", lastname="Doe", age=25, interests={"Lua"}, nothing = nil}
arrayprint(a)
print()
--pairs可以遍历表中所有的key，
--并且除了迭代器本身以及遍历表本身还可以返回nil;
--但是ipairs则不能返回nil,只能返回数字0，如果遇到nil则退出。
--它只能遍历到表中出现的第一个不是整数的key
 
tbl = {"alpha", "beta", [3] = "uno", ["two"] = "dos"}  
  
for i,v in ipairs(tbl) do    --输出前三个  
    print( tbl[i] )  
end  
print()
for i,v in pairs(tbl) do    --全部输出  
    print( tbl[i] )  
end  
