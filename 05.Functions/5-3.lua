#!/usr/bin/lua
-- file: 5-3.lua
-- Exercise 5.3: Write a function that receives an 
-- arbitrary number of values and returns all of them
-- except the first one

function exceptthefirst(...)
	arg = table.pack(...)
	for i = 2, arg.n do
		io.write(string.format("%d ", arg[i]))
	end
end

exceptthefirst(1, 2, 3, 4, 5, 6)
