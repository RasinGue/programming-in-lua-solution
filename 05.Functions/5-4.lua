#!/usr/bin/lua
-- file: combinations.lua
-- Exercise 5.4: Write a function that receives an array 
--and prints all combinations of the elements in the array.

function combinations(a, r)
	if r > #a then -- 具有在一个集合中超过元素个数的组合数
		return {}
	end
	
	if r == 0 then -- 没有元素在集合中
		return {}
	end
	
	if r == 1 then -- 单个元素时
		local return_table = {}
		for i = 1, #a do
			table.insert(return_table, {a[i]}) --插入单元素集合
		end
		return return_table
	else
		local return_table = {}
		local a_new = {} --建立新的集合表
		for i = 2, #a do
			table.insert(a_new, a[i]) 
		end
		for i, v in pairs(combinations(a_new, r-1)) do -- 递归
			local curr_result = {}
			table.insert(curr_result, a[1]);
			for j,curr_val in pairs(v) do
				table.insert(curr_result, curr_val)
			end
			table.insert(return_table, curr_result)
		end
		for i, val in pairs(combinations(a_new, r)) do
			table.insert(return_table, val)
		end
		return return_table
	end
end

function all_combination(a)
	for i = 1, #a do
		for i, v in pairs(combinations(a, i)) do
			for j, combination in pairs(v) do
				if(j == #v) then
					io.write(combination)
				else
					io.write(combination .. ",")
				end
			end
			print()
		end
	end
end

print("enter array items, when done enter 'end'")
a = {}
repeat
	a[#a + 1] = item or nil
	item = io.read()
until item == "end"

print("All possible combinations")
all_combination(a)
