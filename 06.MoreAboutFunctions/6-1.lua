#!/usr/bin/lua

-- file: 6-1.lua
-- Exercise 6.1: Write a function integral that receives 
--a function f and returns an approximation of its integral

function integral(func, start, stop, delta)
	local delta = delta or 1e-4
	local int = 0
	for i = start, stop, delta do
		int = int + func(i) * delta
	end
	return int
end

function f(x)
	return x ^ 2 + 2 * x + 3
end

print(integral(f, 1, 10, 1))

