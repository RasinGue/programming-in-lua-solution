#!/usr/bin/lua
-- file: 6-2.lua
-- Exercise 6.2: Exercise 3.3 asked you to write a function that
-- receives a polynomial (represented as a table) and a value
-- for its variable, and returns the polynomial value.
-- Write the curried version of that function. Your functions
-- should receive a polynomial and returns a function that, when
-- called with a value for x, returns the value of the polynomial
-- for that x.

function poly(co)
	return function(x)
		sum = 0
		cur_x = 1
		for i = #co, 1, -1 do
			sum = sum + cur_x * co[i]
			if i ~= 1 then
				cur_x = cur_x * x 
			end
		end
		return sum
	end
end

f = poly{3, 0, 1}
print(f(0))
